package com.javatechie.rabbitmq.demo.publisher;

import com.javatechie.rabbitmq.demo.config.MessagingConfiguration;
import com.javatechie.rabbitmq.demo.dto.Activity;
import com.javatechie.rabbitmq.demo.dto.ActivityStatus;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@RestController

@RequestMapping("/activity")
public class ActivityProducer {

    @Autowired
    private RabbitTemplate template;

    @PostMapping()
    public String bookOrder(@RequestBody Activity activity){
        activity.setActivityId(UUID.randomUUID());

        ActivityStatus activityStatus = new ActivityStatus();
        activityStatus.setActivity(activity);
        activityStatus.setMessage("Succes");
        activityStatus.setStatus("Processed");

        template.convertAndSend(MessagingConfiguration.EXCHANGE,MessagingConfiguration.ROUTING_KEY, activityStatus);

        return "Succes !";
    }

    @GetMapping()
    public String patientActivity() throws IOException, ParseException, InterruptedException {

        File activities = new File("E:\\faculta\\an4\\SD\\DS2020_30641_Florean_Alexandru_Assigment2\\DS2020_30641_Florean_Alexandru_Assigment2_Producer\\activity.txt");
        BufferedReader buffer = new BufferedReader(new FileReader(activities));

        String text;
        Activity activity = new Activity();
        activity.setActivityId(UUID.fromString("926aefd5-a5dc-4793-a6b4-e71eefbd3a0c"));

        while ((text = buffer.readLine()) != null) {

            String[] spliter = text.split("\t\t");
            SimpleDateFormat yaBoy = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date start = yaBoy.parse(spliter[0]);
            Date end = yaBoy.parse(spliter[1]);

            activity.setName(spliter[2]);
            activity.setStart(start);
            activity.setEnd(end);

            ActivityStatus activityStatus = new ActivityStatus();
            activityStatus.setActivity(activity);
            activityStatus.setMessage("This is the message");
            activityStatus.setStatus("Received");

            template.convertAndSend(MessagingConfiguration.EXCHANGE,MessagingConfiguration.ROUTING_KEY, activityStatus);
            Thread.sleep(1000);
        }

        return "Succes !";
    }
}
